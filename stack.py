from __future__ import print_function
from troposphere import Template, Ref, Parameter, Join, GetAtt
from troposphere.constants import NUMBER
from troposphere.s3 import Bucket, PublicRead
from troposphere.iam import AccessKey, Group, LoginProfile, PolicyType
from troposphere.iam import User, UserToGroupAddition, Role, Policy

from troposphere.awslambda import Function, Code, MEMORY_VALUES

t = Template()

t.add_resource(Bucket("Timestamp", AccessControl=PublicRead,))

cfnuser = t.add_resource(User(
    "CFNUser",
    LoginProfile=LoginProfile(Password="Password"))
)

cfnusergroup = t.add_resource(Group("CFNUserGroup"))
cfnadmingroup = t.add_resource(Group("CFNAdminGroup"))

cfnkeys = t.add_resource(AccessKey(
    "CFNKeys",
    Status="Active",
    UserName=Ref(cfnuser))
)

users = t.add_resource(UserToGroupAddition(
    "Users",
    GroupName=Ref(cfnusergroup),
    Users=[Ref(cfnuser)],
))

admins = t.add_resource(UserToGroupAddition(
    "Admins",
    GroupName=Ref(cfnadmingroup),
    Users=[Ref(cfnuser)],
))

cfnpolicies = t.add_resource(PolicyType(
    "CFNUserPolicies",
    PolicyName="CFNUsers",
    Groups=[Ref(cfnadmingroup)],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [{
            "Effect": "Allow",
            "Action": [
                "cloudformation:Describe*",
                "cloudformation:List*",
                "cloudformation:Get*",
		"cloudformation:Create*",
		"cloudformation:ValidateTemplate"
            ],
            "Resource": "*"
        }],
    }
))


MemorySize = t.add_parameter(Parameter(
    'LambdaMemorySize',
    Type=NUMBER,
    Description='Amount of memory to allocate to the Lambda Function',
    Default='128',
    AllowedValues=MEMORY_VALUES
))

Timeout = t.add_parameter(Parameter(
    'LambdaTimeout',
    Type=NUMBER,
    Description='Timeout in seconds for the Lambda function',
    Default='60'
))


code = [
	"exports.handler = (event, context, callback) => {",
	"    console.log('LogScheduledEvent');",
	"    console.log('Received event:', JSON.stringify(event, null, 2));",
	"    callback(null, 'Finished');",
	"};"
]

LambdaExecutionRole = t.add_resource(Role(
    "LambdaExecutionRole",
    Path="/",
    AssumeRolePolicyDocument={
	"Version": "2012-10-17",
	"Statement": [{
	    "Action": ["sts:AssumeRole"],
	    "Effect": "Allow",
	    "Principal": {
		"Service": ["lambda.amazonaws.com"]
	    }
	}]
    },
))

AppendItemToListFunction = t.add_resource(Function(
    "AppendItemToListFunction",
    Code=Code(
	ZipFile=Join("", code)
    ),
    Handler="index.handler",
    Role=GetAtt("LambdaExecutionRole", "Arn"),
    Runtime="nodejs6.10",
    MemorySize=Ref(MemorySize),
    Timeout=Ref(Timeout)
))


print(t.to_json())
