# Healthfinch interview

## Requirements

* Python and PIP
* Troposphere
* awscli

## Package installation

```
pip install troposphere --user
pip install --upgrade --user awscli
```

## Usage

After installing awscli you will need to configure it wby running `aws configure` this assumes that you have `~/.local` in your path.

Modify `stack.py` to contain the AWS resource requirements. Then validate the Cloud Formation template by running `./validatestack`. You may get an output that `CAPABILITY_IAM` needs to be specified. This specification is handled by the code for creating a stack. You can run `./createstack` to create a stack on AWS.
